1. Tìm feature ngoài việt nam (http://postgis.net/workshops/postgis-intro/spatial_relationships.html)

```
SELECT * FROM public.qh_vn WHERE ST_Disjoint(geom, ST_MakeEnvelope(101.798104, 6.548273, 111.596602, 24.815661, 4326)) limit 1000;

SELECT quan_huyen, "tinh_TP", ST_AsGeoJSON(geom)
```

2. Tính bounding box cho bảng

```
SELECT ST_Extent(geom) FROM qh_vn;
```

3. Update dữ liệu 2D

```
UPDATE qh_vn SET geom = ST_Force2D(geom);
UPDATE ht_vn SET geom = ST_Force2D(geom);

<!-- check data -->
select ST_AsGeoJSON(geom) from ht_vn where gid = 7200038;
```
