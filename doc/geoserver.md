## Geoserver layers

1. Layer quy hoạch các huyện theo định dạng `QH_[tỉnh]_[huyện]`
   QH_ANGIANG_HUYENANPHU
   QH_ANGIANG_HUYENTRITON

2. Layer quy hoạch các tỉnh theo định dạng `QH_[tỉnh]`
   QH_ANGIANG

3. Layer quy hoạch màu hồng của cả nước

```
const mywms = L.tileLayer.wms(
"http://localhost:8080/geoserver/gwc/service/wms",
{
    layers: "meey_map:QH_VN",
    format: "image/png",
    transparent: true,
});
mywms.addTo(map);
```

4. Layer quy hoạch từng quận huyện

```
const mywms = L.tileLayer.wms(
"http://localhost:8080/geoserver/gwc/service/wms",
{
    layers: "meey_map:QH_ANGIANG_HUYENANPHU",
    format: "image/png",
    transparent: true,
    // thêm bounds để leaflet không gửi request dến server nếu ngoài bounds
    bounds: [
        new L.LatLng(23.164298380923384, 107.3924162524909),
        new L.LatLng(21.19487172691199, 105.22418705540439),
    ],
}
);
mywms.addTo(map);
```

5. Lấy feature theo polygon

```
const coordinates = [[[105.099421,10.845951],[105.115557,10.846962],[105.104227,10.833643],[105.099421,10.845951]]];
const cql_filter = `INTERSECTS(geom, POLYGON((${coordinates[0]
    .map((a) => a[1] + " " + a[0])
    .join(",")})))`;

var wfsParams = {
    service: "WFS",
    request: "GetFeature",
    version: "2.0.0",
    typeName: "meey_map:QH_VN",
    outputFormat: "application/json",
    srsName: "EPSG:4326",
    cql_filter: cql_filter,
    count: 50,
};

axios.get('http://localhost:8080/geoserver/wfs', wfsParams);
```

```
curl --location --request POST 'https://geo-test-v2.meey.dev/geoserver/wfs' \
    --header 'authorization: Basic Z2VvLXRvb2w6ZzNPdG9vbDE=' \
    --header 'content-type: application/x-www-form-urlencoded' \
    --data-urlencode 'service=WFS' \
    --data-urlencode 'request=GetFeature' \
    --data-urlencode 'version=2.0.0' \
    --data-urlencode 'typeName=meeymap:QH_VN' \
    --data-urlencode 'outputFormat=application/json' \
    --data-urlencode 'cql_filter=INTERSECTS(geom, SRID=4326;POLYGON ((106.73479719 20.7732478, 106.7347548 20.77329489, 106.73471156 20.77326097, 106.73467756 20.77323431, 106.73470616 20.77320521, 106.73475796 20.77315249, 106.73478772 20.77312221, 106.734854 20.77317316, 106.73479719 20.7732478)))' \
    --data-urlencode 'count=20'
```

6. Layer KHSDD (https://www.geowebcache.org/docs/current/services/tms.html)

```
https://geo-test-v2.meey.dev/geoserver/gwc/service/tms/1.0.0/meeymap%3AKHSDD_VN/z/x/y.png8

https://geo-test-v2.meey.dev/geoserver/gwc/service/tms/1.0.0/meeymap%3AKHSDD_VN/10/815/572.png8
```
