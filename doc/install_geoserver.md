## Cài đặt geoserver

1. Đảm bảo rằng bạn đã cài đặt Java Runtime Environment (JRE) trên hệ thống của mình. GeoServer yêu cầu môi trường Java 11 hoặc Java 17, có sẵn từ OpenJDK, Adoptium hoặc được cung cấp bởi bản phân phối hệ điều hành của bạn
2. Điều hướng đến trang Tải xuống GeoServer.
3. Chọn phiên bản GeoServer mà bạn muốn tải xuống. Nếu bạn không chắc chắn, hãy chọn Bản phát hành ổn định
4. Chọn file nền tảng trên trang tải xuống: geoserver-2.23-SNAPSHOT-bin.zip
5. Tải xuống tệp nén zip và giải nén vào thư mục mà bạn muốn đặt chương trình
6. Thêm một biến môi trường để lưu vị trí của GeoServer bằng cách gõ lệnh sau:

```
echo "export GEOSERVER_HOME=/usr/share/geoserver" >> ~/.profile
. ~/.profile
```

7. Hãy tự đặt bạn làm chủ sở hữu của thư mục máy chủ địa lý. Nhập lệnh sau vào cửa sổ dòng lệnh, thay thế USER_NAME bằng tên người dùng của riêng bạn:

```
sudo chown -R USER_NAME /usr/share/geoserver/
```

8. Khởi động GeoServer bằng cách thay đổi vào thư mục geoserver / bin và thực thi tập lệnh startup.sh:

```
cd geoserver/bin
sh startup.sh
```

9. Trong trình duyệt web, điều hướng đến http: // localhost: 8080 / geoserver.
   Nếu bạn thấy trang Chào mừng của GeoServer, thì GeoServer đã được cài đặt thành công.

10. Để tắt GeoServer, hãy đóng cửa sổ dòng lệnh liên tục hoặc chạy tệp shutdown.sh bên trong thư mục bin.
