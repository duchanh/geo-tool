## Deploy on

1. Install packages

```bash
npm install
```

2. Create file config .env.local in root folder

```
# key use to login
AUTH_USER = sfjsldkwioeurWER34dfgdf

# postgresql config
PG_HOST = 127.0.0.1
PG_PORT = 5432
PG_USERNAME = postgres
PG_PASSWORD = 123456
PG_DATABASE = osm
PG_SCHEMA = public

# geoserver config
GEOSERVER_USER = admin
GEOSERVER_PASS = geoserver
GEOSERVER_URL = http://localhost:8080/geoserver
GEOSERVER_WORKSPACE = meeymap
GEOSERVER_STORE = meey
GEOSERVER_FOLDER = /home/woody/Documents/geoserver

# tool server config
NEXT_PUBLIC_BASE_PATH = /geoserver
NEXT_PUBLIC_MAX_UPLOAD_FILES = 10
NEXT_PUBLIC_MAX_FILE_SIZE = 209715200 # 200 MB

# upload file
UPLOAD_FILE_HOST = http://localhost:3000/geoserver/api/file
```

3. Build tool

```bash
npm run build
```

4. Run server tool (should start in screen)

```bash
npm start
```

## Hướng dẫn sử dụng

1. Đến địa chỉ http://localhosst:3000 để upload file
2. Server sử dụng Nextjs (xem thêm https://nextjs.org/)

## Thiết kế database

1. Database được thiết kế theo yêu cầu các trường do bên địa lý cung cấp > [link](https://docs.google.com/document/d/1wVjSbThpgnMIe-T4LJlfROV3p5cwmGl1/edit?usp=sharing&ouid=106156233868579020495&rtpof=true&sd=true)
