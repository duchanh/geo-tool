import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { apiClient } from "../lib/http_client";

const FileQuyetDinhStats = () => {
  const [data, setData] = useState<any>();

  const fetchData = async (params: any) => {
    if (data) {
      setData(null);
    }
    const resp: any = await apiClient.get("/api/file_sync", {
      params: params,
    });
    setData(resp.data);
  };

  useEffect(() => {
    const interval = setInterval(() => fetchData({}), 5000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div>
      <p>
        <Button
          variant="outline-success"
          size="sm"
          onClick={() => fetchData({ init: true })}
        >
          Khởi tạo
        </Button>
        <Button
          className="ms-2"
          variant="outline-danger"
          size="sm"
          onClick={() => fetchData({ sync: true })}
        >
          Đồng bộ
        </Button>
      </p>
      <Table hover size="sm" className="mt-5">
        <thead>
          <tr>
            <th>#</th>
            <th>file</th>
            <th>Update count</th>
          </tr>
        </thead>
        <tbody>
          {data?.tasks.map((task: any, index: any) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{task.file}</td>
              <td>{task.count}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default FileQuyetDinhStats;
