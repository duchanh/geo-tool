import { Button } from "react-bootstrap";
import { apiClient } from "../lib/http_client";

export const SeedLayer = ({ layer }: { layer: string }) => {
  const seed = async () => {
    const resp = await apiClient.post("/api/cache", {
      layer: layer,
    });
    if (resp.data.error) {
      alert("Lỗi " + resp.data.error);
    } else {
      alert("Tạo cache thành công");
    }
  };
  return (
    <Button
      variant="outline-primary"
      size="sm"
      className="ms-2"
      onClick={() => seed()}
    >
      Tạo cache
    </Button>
  );
};
