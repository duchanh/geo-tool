import Link from "next/link";
import { Button } from "react-bootstrap";
import { apiClient } from "../lib/http_client";
import { SeedLayer } from "./Seed";
import { TaskTable } from "./TaskTable";

export const PreviewKmzLayer = ({
  layer,
  name,
}: {
  layer?: string;
  name?: string;
}) => {
  const removeStore = async () => {
    const oke = confirm("Bạn có chắc muốn xóa layer " + layer);
    if (oke) {
      const resp = await apiClient.delete("/api/kmz", {
        params: { layer: layer },
      });
      if (resp.data.error) {
        alert("Lỗi " + resp.data.error);
      } else {
        alert("Xóa thành công");
      }
    }
  };
  return layer ? (
    <span>
      <Link href={`/preview?layer=${layer}&type=coverage`}>
        {name || layer}
      </Link>
      <Button
        variant="outline-danger"
        size="sm"
        className="ms-2"
        onClick={() => removeStore()}
      >
        Xóa
      </Button>
      <SeedLayer layer={layer} />
    </span>
  ) : (
    <span>{name || layer}</span>
  );
};

export const KmzTaskTable = () => {
  return (
    <TaskTable
      type="kmz"
      columns={[
        {
          title: "Layer",
          render: (item) =>
            item.layerName && <PreviewKmzLayer layer={item.layerName} />,
        },
      ]}
    />
  );
};
