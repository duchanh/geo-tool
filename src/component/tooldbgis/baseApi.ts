import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { PATH_LOCATON } from "./const";

export interface BaseApiParams {
  path?: string;
  baseUrl: string | undefined;
  data?: any;
  contentType?: string;
  noUseToken?: boolean;
  onUploadProgress?: (progressEvent: ProgressEvent) => void;
  headerArgs?: Object;
  keyApi?: string;
}

const getBasicToken = () => {
  const auth = Buffer.from(
    `${`${process.env.BASIC_AUTH_USERNAME}`}:${process.env.BASIC_AUTH_PASSWORD}`
  ).toString("base64");

  return `Basic ${auth}`;
};

const getGetConfig = (noUseToken?: boolean, data?: any) => {
  const config: any = noUseToken
    ? {
        headers: {
          noUseToken,
        },
      }
    : {
        headers: {
          Authorization: getBasicToken(),
        },
      };
  if (data) config.params = data;
  return config;
};

let cancelFlag: any;

let controller: any;

const getPostConfig = (
  contentType?: string,
  noUseToken?: boolean,
  onUploadProgress?: any,
  headerArgs?: any
): AxiosRequestConfig => {
  cancelFlag = axios.CancelToken.source();
  controller = new AbortController();
  const config: AxiosRequestConfig = {
    headers: {
      Authorization: getBasicToken(),
    },
    cancelToken: cancelFlag.token,
    signal: controller.signal,
    onUploadProgress: onUploadProgress,
  };
  if (noUseToken) {
    config?.headers = {
      ...config.headers,
      noUseToken,
    };
  }
  return config;
};

export const baseGetApi = async <T>({
  path = "/",
  baseUrl,
  noUseToken,
  data,
}: BaseApiParams): Promise<T | undefined | null> => {
  try {
    const response: AxiosResponse<T> = await axios.get(
      `${baseUrl}/${path}`,
      getGetConfig(noUseToken, data)
    );
    if (response.status === 200) {
      const { data } = response;
      return data ?? null;
    }
    return null;
  } catch (error: any) {
    throw error?.response?.data ?? null;
  }
};

export const basePostApi = async <T>({
  path = "/",
  baseUrl,
  data,
  contentType,
  noUseToken,
  onUploadProgress,
  headerArgs,
  keyApi,
}: BaseApiParams): Promise<T | any> => {
  if (typeof cancelFlag != typeof undefined && keyApi === "video") {
    cancelFlag.cancel("Operation canceled due to new request.");
  }
  try {
    const response: AxiosResponse<T, any> = await axios.post(
      `${baseUrl}/${path}`,
      data,
      getPostConfig(contentType, noUseToken, onUploadProgress, headerArgs)
    );
    if (response.status === 200 || response.status === 201) {
      return response.data ?? null;
    }
    return null;
  } catch (error: any) {
    throw error;
  }
};

export const basePatchApi = async <T>({
  path = "/",
  baseUrl,
  data,
  contentType,
  noUseToken,
  onUploadProgress,
  headerArgs,
}: BaseApiParams): Promise<T | any> => {
  try {
    const response: AxiosResponse<T, any> = await axios.patch(
      `${baseUrl}/${path}`,
      data,
      getPostConfig(contentType, noUseToken, onUploadProgress, headerArgs)
    );
    if (response.status === 200 || response.status === 201) {
      return response.data ?? null;
    }
    return null;
  } catch (error: any) {
    if (axios.isCancel(error)) return error.message;
    else {
      throw error;
    }
  }
};

export const getFullListCity = async () => {
  try {
    const res: any = await baseGetApi({
      path: PATH_LOCATON["city"].path,
      baseUrl: process.env.BASE_DOMAIN_LOCATION,
      data: {
        limit: 100,
      },
    });

    const data = res.results
      .map((item: any) => {
        return {
          ...item,
          label: item.translation[0].name,
          value: item._id,
        };
      })
      .sort((a: any, b: any) => {
        const textA = a.label.toUpperCase();
        const textB = b.label.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

    return data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

export const getFullListDistrict = async ({ city }: any) => {
  try {
    const res: any = await baseGetApi({
      path: PATH_LOCATON["district"].path,
      baseUrl: process.env.BASE_DOMAIN_LOCATION,
      data: {
        limit: 500,
        city: city,
      },
    });

    const data = res.results
      .map((item: any) => {
        return {
          ...item,
          label: item.translation[0].name,
          value: item._id,
        };
      })
      .sort((a: any, b: any) => {
        const textA = a.label.toUpperCase();
        const textB = b.label.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

    return data;
  } catch (err) {
    console.log(err);
    return [];
  }
};
