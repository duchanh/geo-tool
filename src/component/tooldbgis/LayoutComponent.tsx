import { Layout } from "antd";
import clsx from "clsx";
import { useState } from "react";

const headerStyle: React.CSSProperties = {
  color: "#fff",
  height: 64,
  backgroundColor: "#22d3ee",
};

interface IItem {
  label: string;
  key: string;
}

const items: IItem[] = [
  {
    label: "Dữ liệu quy hoạch",
    key: "planningData",
  },
  {
    label: "Danh mục",
    key: "list",
  },
];

export interface Ilayout {
  children: React.ReactNode;
}

const { Header, Content } = Layout;

const WrapLayout = ({ children }: Ilayout) => {
  const [activeKey, setActiveKey] = useState("");

  return (
    <Layout className="min-h-[100vh]">
      <Header style={headerStyle} className="flex">
        {items.map((item: IItem) => (
          <div
            key={item.key}
            className={clsx(
              "cursor-pointer hover:border-blue-700 hover:border-b-2 px-4 hover:text-white",
              activeKey === item.key && "border-blue-700 border-b-2"
            )}
          >
            <div
              onClick={() => {
                setActiveKey(item.key);
              }}
              className={clsx("font-medium text-[16px] no-underline")}
            >
              {item.label}
            </div>
          </div>
        ))}
      </Header>
      <Content className="p-4 h-full">{children}</Content>
    </Layout>
  );
};

export default WrapLayout;
