import { convertTime, getHour } from "./utils";
import { FieldsHistory, HistoryAction } from "./const";

export const columnsCity = [
  {
    title: "STT",
    dataIndex: "number",
    key: "number",
  },
  {
    title: "ID Tỉnh/Thành phố",
    dataIndex: "_id",
    key: "_id",
  },
  {
    title: "Tên Tỉnh/Thành phố",
    dataIndex: "translation",
    key: "translation",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value[0].prefix} {value[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value[1].prefix} {value[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Trạng thái",
    dataIndex: "isActive",
    key: "isActive",
    render: (value: boolean) => (
      <div>
        {value ? (
          <span className="text-green-500 font-medium">Đang hiệu lực</span>
        ) : (
          <span>Hết hiệu lực</span>
        )}
      </div>
    ),
  },
  {
    title: "Mã bưu chính",
    dataIndex: "postalCode",
    key: "postalCode",
  },
  {
    title: "Ghi chú",
    dataIndex: "note",
    key: "noe",
  },
  {
    title: "Thao tác",
    dataIndex: "actions",
    key: "actions",
  },
];

export const columnsDistrict = [
  {
    title: "STT",
    dataIndex: "number",
    key: "number",
  },
  {
    title: "Mã Quận / Huyện",
    dataIndex: "code",
    key: "code",
  },
  {
    title: "ID Quận / Huyện",
    dataIndex: "_id",
    key: "_id",
  },
  {
    title: "Tên Quận / Huyện",
    dataIndex: "translation",
    key: "translation",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value[0].prefix} {value[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value[1].prefix} {value[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Tỉnh/Thành phố",
    dataIndex: "city",
    key: "city",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Trạng thái",
    dataIndex: "isActive",
    key: "isActive",
    render: (value: boolean) => (
      <div>
        {value ? (
          <span className="text-green-500 font-medium">Đang hiệu lực</span>
        ) : (
          <span>Hết hiệu lực</span>
        )}
      </div>
    ),
  },
  {
    title: "Ghi chú",
    dataIndex: "note",
    key: "noe",
  },
  {
    title: "Thao tác",
    dataIndex: "actions",
    key: "actions",
  },
];

export const columnsWard = [
  {
    title: "STT",
    dataIndex: "number",
    key: "number",
  },
  {
    title: "Mã Phường / Xã",
    dataIndex: "code",
    key: "code",
  },
  {
    title: "ID Phường / Xã",
    dataIndex: "_id",
    key: "_id",
  },
  {
    title: "Tên Phường / Xã",
    dataIndex: "translation",
    key: "translation",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.[0].prefix} {value?.[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.[1].prefix} {value?.[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Tỉnh/Thành phố",
    dataIndex: "city",
    key: "city",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Quận / Huyện",
    dataIndex: "district",
    key: "district",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Trạng thái",
    dataIndex: "isActive",
    key: "isActive",
    render: (value: boolean) => (
      <div>
        {value ? (
          <span className="text-green-500 font-medium">Đang hiệu lực</span>
        ) : (
          <span>Hết hiệu lực</span>
        )}
      </div>
    ),
  },
  {
    title: "Ghi chú",
    dataIndex: "note",
    key: "noe",
  },
  {
    title: "Thao tác",
    dataIndex: "actions",
    key: "actions",
  },
];

export const columnsHistory = [
  {
    title: "Ngày giờ",
    dataIndex: "createdAt",
    key: "createdAt",
    render: (val: any) => (
      <div className="w-36">
        {convertTime(val)} {getHour(val)}
      </div>
    ),
  },
  {
    title: "Trường chỉ tiêu",
    dataIndex: "fields",
    key: "fields",
    render: (val: any[]) => {
      return (
        <div className="w-36">
          {val.map((item, index) => (
            <div key={index}>
              {FieldsHistory[item as keyof typeof FieldsHistory]}
            </div>
          ))}
        </div>
      );
    },
  },
  {
    title: "Gía trị cũ",
    dataIndex: "oldValue",
    key: "oldValue",
    render: (val: any[]) => {
      return val.map((item, index) => {
        if (typeof item === "object" || Array.isArray(item)) {
          return <div key={index}>{JSON.stringify(item)}</div>;
        } else {
          return <div key={index}>{item}</div>;
        }
      });
    },
  },
  {
    title: "Gía trị mới",
    dataIndex: "newValue",
    key: "newValue",
    render: (val: any[]) => {
      return val.map((item, index) => {
        if (typeof item === "object" || Array.isArray(item)) {
          return <div key={index}>{JSON.stringify(item)}</div>;
        } else {
          return <div key={index}>{item}</div>;
        }
      });
    },
  },

  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
    render: (val: string | number) => (
      <>{HistoryAction[val as keyof typeof HistoryAction]}</>
    ),
  },
  {
    title: "Người thực hiện",
    dataIndex: "createdBy",
    key: "createdBy",
  },
];

export const columnsStreet = [
  {
    title: "STT",
    dataIndex: "number",
    key: "number",
  },
  {
    title: "ID Đường",
    dataIndex: "_id",
    key: "_id",
  },
  {
    title: "Tên Đường",
    dataIndex: "translation",
    key: "translation",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.[0].prefix} {value?.[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.[1].prefix} {value?.[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Tỉnh/Thành phố",
    dataIndex: "city",
    key: "city",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Quận / Huyện",
    dataIndex: "district",
    key: "district",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Đường Gộp",
    dataIndex: "streetParent",
    key: "streetParent",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Trạng thái",
    dataIndex: "isActive",
    key: "isActive",
    render: (value: boolean) => (
      <div>
        {value ? (
          <span className="text-green-500 font-medium">Đang hiệu lực</span>
        ) : (
          <span>Hết hiệu lực</span>
        )}
      </div>
    ),
  },
  {
    title: "Ghi chú",
    dataIndex: "note",
    key: "noe",
  },
  {
    title: "Thao tác",
    dataIndex: "actions",
    key: "actions",
  },
];

export const columnsParent = [
  {
    title: "STT",
    dataIndex: "number",
    key: "number",
  },
  {
    title: "ID Đường",
    dataIndex: "_id",
    key: "_id",
  },
  {
    title: "Tên Đường Gộp",
    dataIndex: "translation",
    key: "translation",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.[0].prefix} {value?.[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.[1].prefix} {value?.[1].name}
        </div>
      </div>
    ),
  },

  {
    title: "Trạng thái",
    dataIndex: "isActive",
    key: "isActive",
    render: (value: boolean) => (
      <div>
        {value ? (
          <span className="text-green-500 font-medium">Đang hiệu lực</span>
        ) : (
          <span>Hết hiệu lực</span>
        )}
      </div>
    ),
  },
  {
    title: "Thao tác",
    dataIndex: "actions",
    key: "actions",
  },
];

export const columnsPatch = [
  {
    title: "STT",
    dataIndex: "number",
    key: "number",
  },
  {
    title: "ID Đường",
    dataIndex: "_id",
    key: "_id",
  },
  {
    title: "Tên Đoạn Đường",
    dataIndex: "translation",
    key: "translation",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.[0].prefix} {value?.[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.[1].prefix} {value?.[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Tỉnh/Thành phố",
    dataIndex: "city",
    key: "city",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Quận / Huyện",
    dataIndex: "district",
    key: "district",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Tên Đường",
    dataIndex: "street",
    key: "street",
    render: (value: any) => (
      <div>
        <div>
          Tên tiếng Việt: {value?.translation[0].prefix}{" "}
          {value?.translation[0].name}
        </div>
        <div>
          Tên tiếng Anh: {value?.translation[1].prefix}{" "}
          {value?.translation[1].name}
        </div>
      </div>
    ),
  },
  {
    title: "Trạng thái",
    dataIndex: "isActive",
    key: "isActive",
    render: (value: boolean) => (
      <div>
        {value ? (
          <span className="text-green-500 font-medium">Đang hiệu lực</span>
        ) : (
          <span>Hết hiệu lực</span>
        )}
      </div>
    ),
  },
  {
    title: "Ghi chú",
    dataIndex: "note",
    key: "noe",
  },
  {
    title: "Thao tác",
    dataIndex: "actions",
    key: "actions",
  },
];
