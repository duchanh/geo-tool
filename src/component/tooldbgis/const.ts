export enum DistrictLevelEnum {
  /**
   * Huyện
   */
  DISTRICT = 1,

  /**
   * Quận
   */
  URBAN_DISTRICT = 2,

  /**
   * Thị xã
   */
  TOWN = 3,

  /**
   * Thành phố
   */
  CITY = 4,
}

export const MIN_MAX_LAT_LNG = {
  LAT: {
    min: -90,
    max: 90,
  },
  LNG: {
    min: -180,
    max: 180,
  },
};

export enum StreetPathTypeEnum {
  /**
   * Đường cao tốc
   */
  FREEWAY = 1,

  /**
   * Đường chuyên dụng
   */
  DEDICATED_ROAD = 2,

  /**
   * Đường đô thị
   */
  URBAN_ROAD = 3,

  /**
   * Đường khác
   */
  ANOTHER_WAY = 4,

  /**
   * Đường quốc lộ
   */
  HIGHWAY = 5,

  /**
   * Đường đất
   */
  LANE_LINE = 6,

  /**
   * Đường đi bộ
   */
  WALKING_PATH = 7,

  /**
   * Đường xe đạp
   */
  BIKE_PATH = 8,
}

enum StreetPathLevelEnum {
  /**
   * Đường phố
   */
  STREET = 1,

  /**
   * Ngõ hẻm
   */
  LANE = 2,

  /**
   * Chưa định danh
   */
  UNKNOWN = 3,

  /**
   * Loại khác
   */
  OTHERS = 4,
}

export const OPTIONS_TYPE_STREET = [
  {
    value: StreetPathTypeEnum.FREEWAY,
    label: "Đường cao tốc",
  },
  {
    value: StreetPathTypeEnum.DEDICATED_ROAD,
    label: "Đường chuyên dụng",
  },
  {
    value: StreetPathTypeEnum.URBAN_ROAD,
    label: "Đường đô thị",
  },
  {
    value: StreetPathTypeEnum.ANOTHER_WAY,
    label: "Đường khác",
  },
  {
    value: StreetPathTypeEnum.HIGHWAY,
    label: "Đường quốc lộ",
  },
  {
    value: StreetPathTypeEnum.LANE_LINE,
    label: "Đường đất",
  },
  {
    value: StreetPathTypeEnum.WALKING_PATH,
    label: "Đường đi bộ",
  },
  {
    value: StreetPathTypeEnum.BIKE_PATH,
    label: "ường xe đạp",
  },
];

export const OPTIONS_LEVEL_STREET_PATH = [
  {
    label: "Đường phố",
    value: StreetPathLevelEnum.STREET,
  },
  {
    label: "Ngõ hẻm",
    value: StreetPathLevelEnum.LANE,
  },
  {
    label: "Chưa định danh",
    value: StreetPathLevelEnum.UNKNOWN,
  },
  {
    label: "Loại khác",
    value: StreetPathLevelEnum.OTHERS,
  },
];

export const LIST_FIELD_CITY = {
  mainList: [
    {
      label: "Mã Tỉnh/Thành phố",
      name: "code",
      type: "text",
    },
    {
      label: "Mã bưu điện",
      name: "postalCode",
      type: "text",
    },
    {
      label: "Tọa độ điểm trung tâm",
      name: "centralPoint",
      children: [
        {
          name: "longitude",
          label: "Longitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LNG.min,
          max: MIN_MAX_LAT_LNG.LNG.max,
        },
        {
          name: "latitude",
          label: "Latitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LAT.min,
          max: MIN_MAX_LAT_LNG.LAT.max,
        },
      ],
      hasChild: true,
    },
    {
      label: "Ghi chú",
      name: "note",
      type: "textarea",
    },
    {
      label: "Level",
      name: "level",
      type: "select",
      required: true,
      options: [
        {
          label: "Tỉnh",
          value: 1,
        },
        {
          label: "Thành phố trung ương",
          value: 2,
        },
      ],
    },
    {
      label: "Trạng thái hoạt động",
      name: "isActive",
      type: "switch",
      required: true,
    },
  ],

  subList: [
    {
      list: [
        {
          label: "Tên tiếng Việt",
          name: "vi-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Tỉnh/Thành phố tiếng Việt",
          name: "vi-prefix",
          type: "text",
        },
      ],
    },
    {
      list: [
        {
          label: "Tên tiếng Anh",
          name: "en-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Tỉnh/Thành phố tiếng Anh",
          name: "en-prefix",
          type: "text",
        },
      ],
    },
  ],
};

export const LIST_FIELD_DISTRICT = {
  mainList: [
    {
      label: "Tỉnh / Thành phố",
      name: "city",
      type: "select",
      options: [],
    },
    {
      label: "Mã Quận/Huyện",
      name: "code",
      type: "text",
    },
    {
      label: "Mã bưu điện",
      name: "postalCode",
      type: "text",
    },
    {
      label: "Tọa độ điểm trung tâm",
      name: "centralPoint",
      children: [
        {
          name: "longitude",
          label: "Longitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LNG.min,
          max: MIN_MAX_LAT_LNG.LNG.max,
        },
        {
          name: "latitude",
          label: "Latitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LAT.min,
          max: MIN_MAX_LAT_LNG.LAT.max,
        },
      ],
      hasChild: true,
    },
    {
      label: "Ghi chú",
      name: "note",
      type: "textarea",
    },
    {
      label: "Level",
      name: "level",
      type: "select",
      required: true,
      options: [
        {
          label: "Huyện",
          value: 1,
        },
        {
          label: "Quận",
          value: 2,
        },
        {
          label: "Thị xã",
          value: 3,
        },
        {
          label: "Thành phố",
          value: 4,
        },
      ],
    },
    {
      label: "Trạng thái hoạt động",
      name: "isActive",
      type: "switch",
      required: true,
    },
  ],

  subList: [
    {
      list: [
        {
          label: "Tên tiếng Việt",
          name: "vi-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Quận / Huyện tiếng Việt",
          name: "vi-prefix",
          type: "text",
        },
      ],
    },
    {
      list: [
        {
          label: "Tên tiếng Anh",
          name: "en-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Quận / Huyện phố tiếng Anh",
          name: "en-prefix",
          type: "text",
        },
      ],
    },
  ],
};

export const LIST_FIELD_WARD = {
  mainList: [
    {
      label: "Tỉnh / Thành phố",
      name: "city",
      type: "select",
      options: [],
    },
    {
      label: "Quận / Huyện",
      name: "district",
      type: "select",
      options: [],
    },
    {
      label: "Mã Quận/Huyện",
      name: "code",
      type: "text",
    },
    {
      label: "Mã bưu điện",
      name: "postalCode",
      type: "text",
    },
    {
      label: "Tọa độ điểm trung tâm",
      name: "centralPoint",
      children: [
        {
          name: "longitude",
          label: "Longitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LNG.min,
          max: MIN_MAX_LAT_LNG.LNG.max,
        },
        {
          name: "latitude",
          label: "Latitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LAT.min,
          max: MIN_MAX_LAT_LNG.LAT.max,
        },
      ],
      hasChild: true,
    },
    {
      label: "Ghi chú",
      name: "note",
      type: "textarea",
    },
    {
      label: "Level",
      name: "level",
      type: "select",
      required: true,
      options: [
        {
          label: "Huyện",
          value: 1,
        },
        {
          label: "Quận",
          value: 2,
        },
        {
          label: "Thị xã",
          value: 3,
        },
        {
          label: "Thành phố",
          value: 4,
        },
      ],
    },
    {
      label: "Trạng thái hoạt động",
      name: "isActive",
      type: "switch",
      required: true,
    },
  ],

  subList: [
    {
      list: [
        {
          label: "Tên tiếng Việt",
          name: "vi-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Quận / Huyện tiếng Việt",
          name: "vi-prefix",
          type: "text",
        },
      ],
    },
    {
      list: [
        {
          label: "Tên tiếng Anh",
          name: "en-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Quận / Huyện phố tiếng Anh",
          name: "en-prefix",
          type: "text",
        },
      ],
    },
  ],
};

export const LIST_FIELD_STREET_PARENT = {
  mainList: [
    {
      label: "Tọa độ điểm trung tâm",
      name: "centralPoint",
      children: [
        {
          name: "longitude",
          label: "Longitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LNG.min,
          max: MIN_MAX_LAT_LNG.LNG.max,
        },
        {
          name: "latitude",
          label: "Latitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LAT.min,
          max: MIN_MAX_LAT_LNG.LAT.max,
        },
      ],
      hasChild: true,
    },
    {
      label: "Trạng thái hoạt động",
      name: "isActive",
      type: "switch",
      required: true,
    },
  ],
  subList: [
    {
      list: [
        {
          label: "Tên tiếng Việt",
          name: "vi-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Đường / Phố tiếng Việt",
          name: "vi-prefix",
          type: "text",
        },
      ],
    },
    {
      list: [
        {
          label: "Tên tiếng Anh",
          name: "en-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Đường / Phố phố tiếng Anh",
          name: "en-prefix",
          type: "text",
        },
      ],
    },
  ],
};

export const LIST_FIELD_STREET = {
  mainList: [
    {
      label: "Tỉnh / Thành phố",
      name: "city",
      type: "select",
      options: [],
      required: true,
    },
    {
      label: "Quận / Huyện",
      name: "district",
      type: "select",
      options: [],
      required: true,
    },
    {
      label: "Đường Gộp",
      name: "streetParent",
      type: "select",
      options: [],
    },
    {
      label: "Tọa độ điểm trung tâm",
      name: "centralPoint",
      children: [
        {
          name: "longitude",
          label: "Longitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LNG.min,
          max: MIN_MAX_LAT_LNG.LNG.max,
        },
        {
          name: "latitude",
          label: "Latitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LAT.min,
          max: MIN_MAX_LAT_LNG.LAT.max,
        },
      ],
      hasChild: true,
    },
    {
      label: "Ghi chú",
      name: "note",
      type: "textarea",
    },
    {
      label: "Trạng thái hoạt động",
      name: "isActive",
      type: "switch",
      required: true,
    },
  ],

  subList: [
    {
      list: [
        {
          label: "Tên tiếng Việt",
          name: "vi-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Đường / Phố tiếng Việt",
          name: "vi-prefix",
          type: "text",
        },
      ],
    },
    {
      list: [
        {
          label: "Tên tiếng Anh",
          name: "en-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Đường / Phố phố tiếng Anh",
          name: "en-prefix",
          type: "text",
        },
      ],
    },
  ],
};

export const LIST_FIELD_STREET_PATH = {
  mainList: [
    {
      label: "Tỉnh / Thành phố",
      name: "city",
      type: "select",
      options: [],
      required: true,
    },
    {
      label: "Quận / Huyện",
      name: "district",
      type: "select",
      options: [],
      required: true,
    },
    {
      label: "Phường / Xã",
      name: "ward",
      type: "select",
      options: [],
    },
    {
      label: "Đường / Phố",
      name: "street",
      type: "select",
      options: [],
      required: true,
    },
    {
      label: "Tọa độ điểm trung tâm",
      name: "centralPoint",
      children: [
        {
          name: "longitude",
          label: "Longitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LNG.min,
          max: MIN_MAX_LAT_LNG.LNG.max,
        },
        {
          name: "latitude",
          label: "Latitude",
          type: "number",
          min: MIN_MAX_LAT_LNG.LAT.min,
          max: MIN_MAX_LAT_LNG.LAT.max,
        },
      ],
      hasChild: true,
    },
    {
      label: "Loại đường",
      name: "type",
      type: "select",
      options: OPTIONS_TYPE_STREET,
    },
    {
      label: "Phân loại đường",
      name: "level",
      type: "select",
      options: OPTIONS_LEVEL_STREET_PATH,
    },
    {
      label: "Thuộc tính đoạn đường",
      name: "properties",
      children: [
        {
          name: "firstPoint",
          label: "Điểm đầu",
          type: "text",
        },
        {
          name: "lastPoint",
          label: "Điểm cuối",
          type: "text",
        },
        {
          name: "osmId",
          label: "Mã đoạn trên dữ liệu GIS",
          type: "text",
        },
        {
          name: "ref",
          label: "Tên khác của đoạn đường",
          type: "text",
        },
      ],
      hasChild: true,
    },
    {
      label: "Trạng thái hoạt động",
      name: "isActive",
      type: "switch",
      required: true,
    },
  ],

  subList: [
    {
      list: [
        {
          label: "Tên tiếng Việt",
          name: "vi-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Đường / Phố tiếng Việt",
          name: "vi-prefix",
          type: "text",
        },
      ],
    },
    {
      list: [
        {
          label: "Tên tiếng Anh",
          name: "en-name",
          type: "text",
          required: true,
        },
        {
          label: "Tiền tố Đường / Phố phố tiếng Anh",
          name: "en-prefix",
          type: "text",
        },
      ],
    },
  ],
};

export enum Language {
  VI = "vi",
  EN = "en",
}

export const PATH_LOCATON = {
  city: {
    path: "v1/admin/city",
    listField: LIST_FIELD_CITY,
  },
  district: {
    path: "v1/admin/district",
    listField: LIST_FIELD_DISTRICT,
  },
  ward: {
    path: "v1/admin/ward",
    listField: LIST_FIELD_WARD,
  },
  "street-parent": {
    path: "v1/admin/street-parent",
    listField: LIST_FIELD_STREET_PARENT,
  },
  street: {
    path: "v1/admin/street",
    listField: LIST_FIELD_STREET,
  },
  "street-path": {
    path: "v1/admin/street-path",
    listField: LIST_FIELD_STREET_PATH,
  },
};

export enum FieldsHistory {
  slug = "slug",
  "code" = "Mã",
  "postalCode" = "Mã Bưu chính",
  "translation" = "Ngôn ngữ",
  "location" = "Vị trí",
  "priority" = "Độ ưu tiên",
  "level" = "Cấp",
  "note" = "Ghi chú",
  "isActive" = "Trạng thái",
  city = "Tỉnh / Thành Phố",
  "district" = "Quận Huyện",
  updatedAt = "thời gian update",
}

export const HistoryAction = {
  /**
   * Thêm
   */
  "1": "Thêm",

  /**
   * Sửa
   */
  "2": "Sửa",

  /**
   * Xoá
   */
  "3": "Xóa",

  /**
   * Upload
   */
  "4": "Upload",
};
