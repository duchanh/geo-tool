import type { RadioChangeEvent } from "antd";
import { Radio, message } from "antd";
import { useRouter } from "next/router";
import { useMemo, useRef, useState } from "react";
import { PATH_LOCATON } from "./const";
import { baseGetApi, basePostApi, basePatchApi } from "./baseApi";
import LabelActions from "./LabelActionsComponent";
import Layout from "./LayoutComponent";
import MeeyTable from "./MeeyTableComponent";
import FormLocation from "./FormLocation";
import {
  columnsCity,
  columnsDistrict,
  columnsWard,
  columnsHistory,
  columnsStreet,
  columnsParent,
  columnsPatch,
} from "./columns";
import { Language } from "./const";
import { EditFilled, HistoryOutlined, LeftOutlined } from "@ant-design/icons";

const ToolDbGis = (props: any) => {
  const { totalResults, results, name, mode, listField, dataField, isHistory } =
    props;
  const [messageApi, contextHolder] = message.useMessage();

  const [currentPage, setCurrentPage] = useState<number>(1);
  const [listData, setListData] = useState(null);
  const [totalCount, setTotalCount] = useState(null);

  const [valueInput, setValueInput] = useState("");
  const [valueCode, setValueCode] = useState("");
  const [valueLevel, setValueLevel] = useState(null);
  const [valueIsActive, setValueIsActive] = useState(true);
  const [valueCity, setValueCity] = useState(null);
  const [valueDistrict, setValueDistrict] = useState(null);

  const refInputFile = useRef<any>(null);

  console.log(results, "wwwww");

  const router = useRouter();

  const columns = useMemo(() => {
    let columns: any[] = [];

    if (isHistory) {
      columns = columnsHistory;
    } else {
      if (name === "city") {
        columns = columnsCity;
      } else if (name === "district") {
        columns = columnsDistrict;
      } else if (name === "ward") {
        columns = columnsWard;
      } else if (name === "street") {
        columns = columnsStreet;
      } else if (name === "street-path") {
        columns = columnsPatch;
      } else if (name === "street-parent") {
        columns = columnsParent;
      }
    }

    return columns.map((cell) => {
      if (cell.key === "actions") {
        return {
          ...cell,
          render: (_: any, record: any) => {
            return (
              <div className="flex">
                <div
                  onClick={() => {
                    router.push(`/tooldbgis/edit/${name}?id=${record._id}`);
                  }}
                  className="cursor-pointer mr-5"
                >
                  <EditFilled size={16} />
                </div>
                <div
                  onClick={() => {
                    router.push(`/tooldbgis/history/${name}?id=${record._id}`);
                  }}
                  className="cursor-pointer"
                >
                  <HistoryOutlined size={16} />
                </div>
              </div>
            );
          },
        };
      }
      return {
        ...cell,
      };
    });
  }, [name]);

  const onChangeTab = (event: RadioChangeEvent) => {
    setListData(null);
    setTotalCount(null);
    router.push(`/tooldbgis/${event.target.value}`);
  };

  const addData = () => {
    router.push(`/tooldbgis/add/${name}`);
  };
  const uploadFile = async () => {
    try {
      const formData = new FormData();
      formData.append("file", refInputFile.current.input.files[0]);
      const res = await basePostApi<any>({
        path: `${PATH_LOCATON[name as keyof typeof PATH_LOCATON].path}/import`,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
        data: formData,
      });
      messageApi.open({
        type: "success",
        content: "Upload file Thành công",
      });
      refInputFile.current.input.value = "";
    } catch (err: any) {
      messageApi.open({
        type: "error",
        content: err.response.data.message,
      });
    }
  };

  const onChangePage = async (page: number) => {
    try {
      const res = await baseGetApi<any>({
        path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
        data: {
          page: page,
          level: valueLevel ? valueLevel : null,
          code: valueCode ? valueCode : null,
          keyword: valueInput ? valueInput : null,
          isActive: valueIsActive ? valueIsActive : null,
          city: valueCity ? valueCity : null,
          district: valueDistrict ? valueDistrict : null,
        },
      });
      setCurrentPage(page);
      const data = res.results.map((item: any, index: number) => {
        return {
          ...item,
          number: page < 2 ? index + 1 : (page - 1) * 10 + index + 1,
        };
      });
      setListData(data);
    } catch (err: any) {
      console.log(err);
    }
  };

  const exportFile = async () => {
    const res = await baseGetApi({
      path: `${PATH_LOCATON[name as keyof typeof PATH_LOCATON].path}/export`,
      baseUrl: process.env.BASE_DOMAIN_LOCATION,
    });
  };

  const submitForm = async (payloadData: any) => {
    if (mode === "edit" && dataField._id) {
      try {
        const res = await basePatchApi({
          path: `${PATH_LOCATON[name as keyof typeof PATH_LOCATON].path}/${
            dataField._id
          }`,
          baseUrl: process.env.BASE_DOMAIN_LOCATION,
          data: payloadData,
        });
        router.push(`/tooldbgis/${name}`);
      } catch (err: any) {
        messageApi.open({
          type: "error",
          content: err.response.data.message,
        });
      }
    } else {
      try {
        const res = await basePostApi({
          path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
          baseUrl: process.env.BASE_DOMAIN_LOCATION,
          data: payloadData,
        });
        router.push(`/tooldbgis/${name}`);
      } catch (err: any) {
        messageApi.open({
          type: "error",
          content: err.response.data.message,
        });
      }
    }
  };

  const getValuesForm = async (values: any) => {
    const payloadData: any = {
      code: values.code ?? null,
      postalCode: values.postalCode,
      isActive: values.isActive,
      centralPoint: [values.longitude, values.latitude],
      note: values.note ?? null,
      translation: Object.values(Language).map((item) => {
        const keyName = `${item}-name`;
        const keyPrefix = `${item}-prefix`;
        return {
          languageCode: item,
          name: values[keyName],
          prefix: values[keyPrefix],
        };
      }),
      level: values.level ?? null,
    };

    const dataStreet: any = {
      centralPoint: [values.longitude, values.latitude],
      translation: Object.values(Language).map((item) => {
        const keyName = `${item}-name`;
        const keyPrefix = `${item}-prefix`;
        return {
          languageCode: item,
          name: values[keyName],
          prefix: values[keyPrefix],
        };
      }),
      isActive: values.isActive,
    };

    if (name === "district") {
      payloadData.city = values.city ?? null;
    } else if (name === "ward") {
      payloadData.city = values.city ?? null;
      payloadData.district = values.district ?? null;
    }

    if (name === "street") {
      dataStreet.city = values.city ?? null;
      dataStreet.district = values.district ?? null;
      dataStreet.streetParent = values.streetParent ?? null;
    } else if (name === "street-path") {
      dataStreet.city = values.city ?? null;
      dataStreet.district = values.district ?? null;
      dataStreet.ward = values.ward ?? null;
      dataStreet.street = values.street ?? null;
      dataStreet.type = values.type ?? null;
      dataStreet.level = values.level ?? null;
      dataStreet.properties = {
        firstPoint: values.firstPoint ?? null,
        lastPoint: values.lastPoint ?? null,
        osmId: values.osmId ?? null,
        ref: values.ref ?? null,
      };
      dataStreet.note = values.note ?? null;
    }

    if (name.includes("street")) {
      submitForm(dataStreet);
    } else {
      submitForm(payloadData);
    }
  };

  return (
    <Layout>
      {contextHolder}
      <>
        <Radio.Group
          value={name}
          onChange={onChangeTab}
          style={{ marginBottom: 16 }}
          size="large"
          className="my-4"
        >
          <Radio.Button value="street-path">Đoạn Đường</Radio.Button>
          <Radio.Button value="street-parent">Đường Gộp</Radio.Button>
          <Radio.Button value="street">Đường</Radio.Button>
          <Radio.Button value="ward">Phường / Xã</Radio.Button>
          <Radio.Button value="district">Quận / Huyện</Radio.Button>
          <Radio.Button value="city">Tỉnh / Thành phố</Radio.Button>
        </Radio.Group>
        {mode === "list" ? (
          <>
            {isHistory ? (
              <>
                <div
                  className="flex mb-3 cursor-pointer items-center text-base font-medium"
                  onClick={() => {
                    router.back();
                  }}
                >
                  <LeftOutlined />
                  <span className="ml-2">Quay lại</span>
                </div>
                <div className="text-base font-medium">
                  Xem lịch sử thay đổi
                </div>
              </>
            ) : (
              <LabelActions
                refInputFile={refInputFile}
                addData={addData}
                uploadFile={uploadFile}
                exportFile={exportFile}
                name={name}
                setListData={setListData}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
                setTotalCount={setTotalCount}
                valueInput={valueInput}
                setValueInput={setValueInput}
                valueCode={valueCode}
                setValueCode={setValueCode}
                valueLevel={valueLevel}
                setValueLevel={setValueLevel}
                valueIsActive={valueIsActive}
                setValueIsActive={setValueIsActive}
                valueCity={valueCity}
                setValueCity={setValueCity}
                valueDistrict={valueDistrict}
                setValueDistrict={setValueDistrict}
              />
            )}
            <MeeyTable
              totalResults={totalCount !== null ? totalCount : totalResults}
              currentPage={currentPage}
              columns={columns}
              onPageChange={onChangePage}
              data={listData ? listData : results}
            />
          </>
        ) : (
          <FormLocation
            listField={listField ? listField : {}}
            getValuesForm={getValuesForm}
            dataField={dataField}
            name={name}
          />
        )}
      </>
    </Layout>
  );
};

export default ToolDbGis;
