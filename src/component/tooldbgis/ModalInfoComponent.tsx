import { Modal, Form, Input, Select, Button } from "antd";

interface IModalInfo {
  openModal: boolean;
  editMode: boolean;
  closeModal: () => void;
}

const ModalInfo = ({ editMode, openModal, closeModal }: IModalInfo) => {
  const { Item } = Form;

  return (
    <Modal
      open={openModal}
      footer={false}
      title={editMode ? "Sửa" : "Thêm Mới"}
      className="modal-info"
      onCancel={() => {
        closeModal();
      }}
    >
      
    </Modal>
  );
};

export default ModalInfo;
