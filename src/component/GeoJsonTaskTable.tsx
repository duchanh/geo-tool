import { Button } from "react-bootstrap";
import { apiClient } from "../lib/http_client";
import cities from "../lib/cities";
import { SeedLayer } from "./Seed";
import { TaskTable } from "./TaskTable";

export const GeojsonLayerActions = ({
  district,
  city,
  featureType,
  onSuccess,
}: {
  district?: string;
  city: string;
  featureType: string;
  onSuccess: () => void;
}) => {
  const removeDistrict = async () => {
    const oke = confirm(
      "Bạn có chắc muốn xóa dữ liệu của " + district || "" + " " + city
    );
    if (oke) {
      const resp = await apiClient.delete("/api/geojson", {
        params: { featureType: featureType, district: district, city: city },
      });
      if (resp.data.error) {
        alert("Lỗi " + resp.data.error);
      } else {
        onSuccess();
        alert("Xóa thành công " + resp.data.count);
      }
    }
  };
  return (
    <span>
      <Button
        variant="outline-danger"
        size="sm"
        onClick={() => removeDistrict()}
      >
        Xóa
      </Button>
      <SeedLayer
        layer={cities.getLayerName(
          featureType
            .split(" ")
            .map((a) => a[0])
            .join("")
            .toUpperCase(),
          city,
          district
        )}
      />
    </span>
  );
};

export const GeoJsonTaskTable = () => {
  return (
    <TaskTable
      type="geojson"
      columns={[
        { title: "Delete", render: (item) => item.deleteCount },
        {
          title: "Insert",
          render: (item) =>
            item.insertTotalCount &&
            `${item.insertCount} / ${item.insertTotalCount}`,
        },
        {
          title: "Note",
          render: (item) =>
            item.note && (
              <span>
                <span className="me-2">
                  {item.note.featureType} - {item.note.district} -{" "}
                  {item.note.city}
                </span>
                <GeojsonLayerActions {...item.note} />
              </span>
            ),
        },
      ]}
    />
  );
};
