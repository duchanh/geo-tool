import axios from "axios";

export const geoClient = axios.create({
  baseURL: process.env.GEOSERVER_URL,
  timeout: 100000,
  auth: {
    username: process.env.GEOSERVER_USER || "",
    password: process.env.GEOSERVER_PASS || "",
  },
});

export const apiClient = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASE_PATH,
});
