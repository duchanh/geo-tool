import db from "./db";

export interface MapItem {
  require: string[];
  attrs: string[];
  lengths: {
    [key: string]: number;
  };
  layerPrefix: string;
  quan: string;
  tinh: string;
  table: string;
  create: Function;
  defaultStyle?: string;
  styles?: string[];
}

export interface GeoJsonItem {
  quan: string;
  tinh: string;
  featureType: string;
  features: any[];
}

const MAP: { [key: string]: MapItem } = {
  "Hiện trạng": {
    require: ["TINHTP", "QUANHUYEN", "Shape_Area"],
    layerPrefix: "HT",
    quan: "QUANHUYEN",
    tinh: "TINHTP",
    table: "ht_vn",
    defaultStyle: "HienTrang",
    attrs: [
      "address",
      "SHBANDO",
      "SHTHUA",
      "DIENTICH",
      "TINHTP",
      "QUANHUYEN",
      "SHAPE_Leng",
      "Shape_Area",
    ],
    lengths: {
      address: 250,
      TINHTP: 250,
      QUANHUYEN: 250,
    },
    create: async () => {
      await db.raw(`SET search_path = ${process.env.PG_SCHEMA}, public`);
      await db.raw(`CREATE TABLE IF NOT EXISTS ${process.env.PG_SCHEMA}.ht_vn 
                (
            "gid" BIGSERIAL PRIMARY KEY,
            "address" character varying(250) COLLATE pg_catalog."default",
            "SHBANDO" numeric,
            "SHTHUA" numeric,
            "DIENTICH" numeric,
            "TINHTP" character varying(250) COLLATE pg_catalog."default",
            "QUANHUYEN" character varying(250) COLLATE pg_catalog."default",
            "SHAPE_Leng" numeric,
            "Shape_Area" numeric,
            geom geometry
        ) 
        TABLESPACE pg_default`);
      await db.raw(
        `CREATE INDEX IF NOT EXISTS ht_vn_geom_idx ON ${process.env.PG_SCHEMA}.ht_vn USING gist (geom) TABLESPACE pg_default`
      );
      await db.raw(
        `CREATE INDEX IF NOT EXISTS ht_vn_tinh_idx ON ${process.env.PG_SCHEMA}.ht_vn USING btree ("TINHTP" COLLATE pg_catalog."default" ASC NULLS LAST) TABLESPACE pg_default`
      );
      await db.raw(
        `CREATE INDEX IF NOT EXISTS ht_vn_quan_huyen_idx ON ${process.env.PG_SCHEMA}.ht_vn USING btree ("QUANHUYEN" COLLATE pg_catalog."default" ASC NULLS LAST) TABLESPACE pg_default`
      );
    },
  },
  "Quy hoạch": {
    require: ["maDoiTuong", "loaiDat", "Shape_Area", "quan_huyen", "tinh_TP"],
    layerPrefix: "QH",
    quan: "quan_huyen",
    tinh: "tinh_TP",
    table: "qh_vn",
    defaultStyle: "QuyHoachChung",
    styles: ["QuyHoach"],
    attrs: [
      "maDoiTuong",
      "loaiDat",
      "danSo",
      "matDoXayDu",
      "tangCao",
      "ghiChu",
      "kyHieuKhuQ",
      "kyHieuOQH",
      "trangThaiQ",
      "kyHieuPhan",
      "canCuPhapL",
      "URLQuyetdi",
      "URLDoan",
      "Shape_Leng",
      "Shape_Area",
      "dienTich",
      "quan_huyen",
      "tinh_TP",
      "ID_doan",
      "donviPD",
      "ngayPD",
      "tyle",
    ],
    lengths: {
      maDoiTuong: 250,
      loaiDat: 250,
      danSo: 250,
      matDoXayDu: 250,
      tangCao: 250,
      ghiChu: 250,
      kyHieuKhuQ: 250,
      kyHieuOQH: 250,
      trangThaiQ: 250,
      kyHieuPhan: 250,
      canCuPhapL: 250,
      URLQuyetdi: 250,
      URLDoan: 250,
      quan_huyen: 250,
      tinh_TP: 250,
      ID_doan: 250,
      donviPD: 250,
      ngayPD: 250,
      tyle: 250,
    },
    create: async () => {
      await db.raw(`SET search_path = ${process.env.PG_SCHEMA}, public`);
      await db.raw(`CREATE TABLE IF NOT EXISTS ${process.env.PG_SCHEMA}.qh_vn 
        (
            "gid" BIGSERIAL PRIMARY KEY,
            "maDoiTuong" character varying(250) COLLATE pg_catalog."default",
            "loaiDat" character varying(250) COLLATE pg_catalog."default",
            "danSo" character varying(250) COLLATE pg_catalog."default",
            "matDoXayDu" character varying(250) COLLATE pg_catalog."default",
            "tangCao" character varying(250) COLLATE pg_catalog."default",
            "ghiChu" character varying(250) COLLATE pg_catalog."default",
            "kyHieuKhuQ" character varying(250) COLLATE pg_catalog."default",
            "kyHieuOQH" character varying(250) COLLATE pg_catalog."default",
            "trangThaiQ" character varying(250) COLLATE pg_catalog."default",
            "kyHieuPhan" character varying(250) COLLATE pg_catalog."default",
            "canCuPhapL" character varying(250) COLLATE pg_catalog."default",
            "URLQuyetdi" character varying(250) COLLATE pg_catalog."default",
            "URLDoan" character varying(250) COLLATE pg_catalog."default",
            "Shape_Leng" numeric,
            "Shape_Area" numeric,
            "dienTich" numeric,
            quan_huyen character varying(250) COLLATE pg_catalog."default",
            "tinh_TP" character varying(250) COLLATE pg_catalog."default",
            "ID_doan" character varying(250) COLLATE pg_catalog."default",
            "donviPD" character varying(250) COLLATE pg_catalog."default",
            "ngayPD" character varying(250) COLLATE pg_catalog."default",
            "tyle" character varying(250) COLLATE pg_catalog."default",
            geom geometry
        ) 
        TABLESPACE pg_default`);
      await db.raw(
        `CREATE INDEX IF NOT EXISTS qh_vn_geom_idx ON ${process.env.PG_SCHEMA}.qh_vn USING gist (geom) TABLESPACE pg_default`
      );
      await db.raw(
        `CREATE INDEX IF NOT EXISTS qh_vn_quan_huyen_idx ON ${process.env.PG_SCHEMA}.qh_vn USING btree ("quan_huyen" COLLATE pg_catalog."default" ASC NULLS LAST) TABLESPACE pg_default`
      );
      await db.raw(
        `CREATE INDEX IF NOT EXISTS qh_vn_tinh_idx ON ${process.env.PG_SCHEMA}.qh_vn USING btree ("tinh_TP" COLLATE pg_catalog."default" ASC NULLS LAST) TABLESPACE pg_default`
      );
    },
  },
  "Giao thông": {
    require: ["quanHuyen", "tinhTP"],
    layerPrefix: "GT",
    quan: "quanHuyen",
    tinh: "tinhTP",
    table: "gt_vn",
    attrs: [
      "id_qHuyen",
      "quanHuyen",
      "id_tinhTP",
      "tinhTP",
      "osm_id",
      "code",
      "highway",
      "name",
      "ref",
      "oneway",
      "maxspeed",
      "layer",
      "bridge",
      "tunnel",
      "id_duong",
      "loaiDuong",
      "maDuong",
      "phanLoai",
      "maPhanLoai",
      "rongTB",
      "rongMax",
      "rongMin",
      "chieuDai",
      "ghiChu",
    ],
    lengths: {
      id_qHuyen: 250,
      quanHuyen: 250,
      id_tinhTP: 250,
      tinhTP: 250,
      osm_id: 250,
      highway: 250,
      name: 250,
      ref: 250,
      oneway: 250,
      bridge: 250,
      tunnel: 250,
      id_duong: 250,
      loaiDuong: 250,
      maDuong: 250,
      phanLoai: 250,
      maPhanLoai: 250,
      ghiChu: 250,
    },
    create: async () => {
      await db.raw(`SET search_path = ${process.env.PG_SCHEMA}, public`);
      await db.raw(`CREATE TABLE IF NOT EXISTS ${process.env.PG_SCHEMA}.gt_vn 
          (
              "gid" BIGSERIAL PRIMARY KEY,
              "id_qHuyen" character varying(250) COLLATE pg_catalog."default",
              "quanHuyen" character varying(250) COLLATE pg_catalog."default",
              "id_tinhTP" character varying(250) COLLATE pg_catalog."default",
              "tinhTP" character varying(250) COLLATE pg_catalog."default",
              "osm_id" character varying(250) COLLATE pg_catalog."default",
              code numeric,
              "highway" character varying(250) COLLATE pg_catalog."default",
              "name" character varying(250) COLLATE pg_catalog."default",
              "ref" character varying(250) COLLATE pg_catalog."default",
              "oneway" character varying(250) COLLATE pg_catalog."default",
              maxspeed numeric,
              layer numeric,
              "bridge" character varying(250) COLLATE pg_catalog."default",
              "tunnel" character varying(250) COLLATE pg_catalog."default",
              "id_duong" character varying(250) COLLATE pg_catalog."default",
              "loaiDuong" character varying(250) COLLATE pg_catalog."default",
              "maDuong" character varying(250) COLLATE pg_catalog."default",
              "phanLoai" character varying(250) COLLATE pg_catalog."default",
              "maPhanLoai" character varying(250) COLLATE pg_catalog."default",
              "rongTB" numeric,
              "rongMax" numeric,
              "rongMin" numeric,
              "chieuDai" numeric,
              "ghiChu" character varying(250) COLLATE pg_catalog."default",
              geom geometry
          ) 
          TABLESPACE pg_default`);
      await db.raw(
        `CREATE INDEX IF NOT EXISTS gt_vn_geom_idx ON ${process.env.PG_SCHEMA}.gt_vn USING gist (geom) TABLESPACE pg_default`
      );
      await db.raw(
        `CREATE INDEX IF NOT EXISTS gt_vn_quan_huyen_idx ON ${process.env.PG_SCHEMA}.gt_vn USING btree ("quanHuyen" COLLATE pg_catalog."default" ASC NULLS LAST) TABLESPACE pg_default`
      );
      await db.raw(
        `CREATE INDEX IF NOT EXISTS gt_vn_tinh_idx ON ${process.env.PG_SCHEMA}.gt_vn USING btree ("tinhTP" COLLATE pg_catalog."default" ASC NULLS LAST) TABLESPACE pg_default`
      );
    },
  },
};

export default MAP;

export async function createTables() {
  const mapTypes = Object.keys(MAP);
  for (let i = 0; i < mapTypes.length; i += 1) {
    const mapData = MAP[mapTypes[i]];
    await mapData.create(db);
  }
}
