import knex from "knex";
import knexPostgis from "knex-postgis";
import * as turf from "@turf/turf";

import MAP, { GeoJsonItem } from "./map";
import { GeoJsonTask } from "./tasks";

const db = knex({
  client: "pg",
  connection: {
    host: process.env.PG_HOST,
    port: parseInt(process.env.PG_PORT || "5432"),
    user: process.env.PG_USERNAME,
    password: process.env.PG_PASSWORD,
    database: process.env.PG_DATABASE,
  },
  searchPath: process.env.PG_SCHEMA
    ? [process.env.PG_SCHEMA, "public"]
    : ["public"],
});

const st = knexPostgis(db);

export async function removeDistrict(
  featureType: string,
  city: string,
  district?: string
) {
  const trx = await db.transaction();
  const mapData = MAP[featureType];
  let query = trx(mapData.table).where(mapData.tinh, city);
  if (district) {
    query = query.where(mapData.quan, district);
  }
  const count = await query.del();
  await trx.commit();
  return count;
}

export async function updateDatabase(data: GeoJsonItem, task: GeoJsonTask) {
  const trx = await db.transaction();
  const mapData = MAP[data.featureType];

  try {
    task.deleteCount = await trx(mapData.table)
      .where(mapData.quan, data.quan)
      .del();

    const insertData = data.features.map((feature) => {
      const item: any = {};
      const attrs = mapData.attrs;
      for (let i = 0; i < attrs.length; i += 1) {
        item[attrs[i]] = feature.properties[attrs[i]];
      }
      if (
        !turf.booleanContains(
          turf.bboxPolygon([101.798104, 6.548273, 111.596602, 24.815661]),
          turf.bboxPolygon(turf.bbox(feature.geometry))
        )
      ) {
        throw Error(
          `Geometry of ${JSON.stringify(item)} ko nằm trong biên giới việt nam`
        );
      }

      const itemsLength = feature.geometry.coordinates[0].flat().length;
      if (itemsLength > 200000) {
        throw Error(
          `Geometry of ${JSON.stringify(
            item
          )} is too big, ${itemsLength} points`
        );
      }
      if (feature.geometry.type !== "MultiPolygon") {
        throw Error(
          `Geometry of ${JSON.stringify(item)} is ${
            feature.geometry.type
          }. Must be MultiPolygon`
        );
      }
      try {
        // drop if not polygon (number of points > 3)
        feature.geometry.coordinates[0] =
          feature.geometry.coordinates[0].filter((i: any) => i.length > 3);

        // remove 3d data
        feature.geometry.coordinates[0] = feature.geometry.coordinates[0].map(
          (item: any) => item.map((x: any) => x.slice(0, 2))
        );

        item.geom = st.geomFromGeoJSON(feature.geometry);
      } catch (e) {
        throw Error(`Geometry of gid ${feature.properties.gid} is invalid`);
      }
      return item;
    });
    task.insertCount = 0;
    task.insertTotalCount = insertData.length;
    while (insertData.length > 0) {
      let chunk = insertData.splice(0, 2000);
      task.insertCount += chunk.length;
      await trx(mapData.table).insert(chunk);
    }
    await trx.commit();
  } catch (e) {
    await trx.rollback();
    throw e;
  }
}

export default db;
