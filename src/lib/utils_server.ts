import formidable from "formidable";
import { NextApiRequest, NextApiResponse } from "next";
import { join } from "path";

const utilsServer = {
  layerKHSDD: "KHSDD_VN",
  uploadDir: join(process.cwd(), "./data"),
  uploadFiles: async (
    req: NextApiRequest,
    res: NextApiResponse,
    handleFile: (file: formidable.File) => any
  ) => {
    if (req.method === "POST") {
      const form = formidable({
        multiples: true,
        maxFileSize: parseInt(process.env.NEXT_PUBLIC_MAX_FILE_SIZE as string),
        maxFiles: parseInt(process.env.NEXT_PUBLIC_MAX_UPLOAD_FILES as string),
      });
      try {
        const files = await new Promise<formidable.File | formidable.File[]>(
          (resolve, reject) => {
            form.parse(req, (err, fields, files) =>
              err ? reject(err) : resolve(files.files)
            );
          }
        );
        let result: any[] = [];
        if (Array.isArray(files)) {
          const handleMultiple = async () => {
            for (const file of files) {
              result.push(await handleFile(file));
            }
          };
          if (handleFile.constructor.name == "AsyncFunction") {
            handleMultiple();
          } else {
            await handleMultiple();
          }
        } else {
          result = [handleFile(files)];
        }
        res.status(200).json({ status: 1, result });
      } catch (err) {
        res.status(400).send(String(err));
      }
    }
  },
};

export default utilsServer;
