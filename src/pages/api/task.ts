import { NextApiRequest, NextApiResponse } from "next";

import { geoJsonTaskManager, kmzTaskManager } from "../../lib/tasks";
import { checkAuth } from "./user";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (checkAuth(req, res)) {
    res
      .status(200)
      .json({
        data:
          req.query.type === "geojson"
            ? geoJsonTaskManager.getTasks()
            : kmzTaskManager.getTasks(),
      });
  }
}
