import { NextApiRequest, NextApiResponse } from "next";
import { checkAuth } from "./user";
import db from "../../lib/db";
import { makeUrl, UPLOAD_DIR } from "./file";
import { readdirSync } from "fs";

const task = {
  isRunning: false,
  data: [] as Array<{ file: string; count: any }>,
};

function initData() {
  var files = readdirSync(UPLOAD_DIR);
  task.data = files.map((f) => ({ file: f, count: null }));
  return files;
}

async function syncAll() {
  const files = initData();
  task.isRunning = true;
  try {
    for (const file of files) {
      const count = await syncQuyetDinh(file);
      const item = task.data.find((d) => d.file === file);
      if (item) {
        item.count = count;
      }
    }
  } catch (e) {
    console.log(e);
  }
  task.isRunning = false;
}

async function updateData(doAn: string, column: string, value: string) {
  const trx = await db.transaction();
  const count = await trx("qh_vn")
    .where("ID_doan", doAn)
    .update({ [column]: value });
  await trx.commit();
  return count;
}

export async function syncQuyetDinh(fileName: string) {
  const parts = fileName.split(".");
  const url = makeUrl(fileName);
  if (parts[0].endsWith("_BV")) {
    return await updateData(parts[0].slice(0, -3), "URLDoan", url);
  } else if (parts[0].endsWith("_QD")) {
    return await updateData(parts[0].slice(0, -3), "URLQuyetdi", url);
  }
  return null;
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (checkAuth(req, res)) {
    if (req.method === "POST") {
      const file: string = req.body.file;
      if (file) {
        const count = await syncQuyetDinh(file);
        return res.json({ count });
      }
    } else if (req.method === "GET") {
      if (req.query.sync) {
        syncAll();
      } else if (req.query.init) {
        initData();
      }
      return res.json({ tasks: task.data });
    }
    res.status(404).send("Not found");
  }
}
