import { useState } from "react";
import { Button, Col, Container, Form, Row, Spinner } from "react-bootstrap";
import { featureTypes } from "../component/GeojsonStats";

import { apiClient } from "../lib/http_client";
import utils from "../lib/utils";

const Layers = ({ storeName }: { storeName: string }) => {
  const [loading, setLoading] = useState(false);
  const [map, setMap] = useState(featureTypes[0]);
  const [store, setStore] = useState(storeName);

  const handle = async (
    method: "post" | "delete" | "createTables" | "syncKHSDD"
  ) => {
    try {
      setLoading(true);
      const start = Date.now();
      let resp;
      if (method === "post") {
        resp = await apiClient.post("/api/layers", { type: map, store });
      } else if (method === "delete") {
        resp = await apiClient.delete("/api/layers", {
          params: { type: map, store },
        });
      } else if (method === "syncKHSDD") {
        resp = await apiClient.patch("/api/kmz");
      } else {
        resp = await apiClient.post("/api/tables");
      }
      const duration = Date.now() - start;
      prompt(
        "Success " + utils.abbreviate(duration, ["ms", "s"]),
        JSON.stringify(resp.data)
      );
    } catch (e: any) {
      alert(e.message + ": " + e?.response?.data);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Container>
      {loading ? (
        <p className="text-center mt-5">
          <Spinner animation="border" variant="primary" />
        </p>
      ) : (
        <Row className="mt-5 align-items-top">
          <Col md={{ span: 3 }}>
            <Form.Select
              placeholder="Type"
              value={map}
              onChange={(e) => setMap(e.target.value)}
            >
              {featureTypes.map((t) => (
                <option value={t} key={t}>
                  {t}
                </option>
              ))}
            </Form.Select>
          </Col>
          <Col md={{ span: 3 }}>
            <Form.Control
              placeholder="Store"
              value={store}
              onChange={(e) => setStore(e.target.value)}
            />
          </Col>
          <Col md={{ span: 6 }}>
            <Button variant="primary" onClick={() => handle("post")}>
              Create
            </Button>
            <Button
              variant="danger"
              className="ms-2"
              onClick={() => handle("delete")}
            >
              Delete
            </Button>
            <Button
              variant="warning"
              className="ms-2"
              onClick={() => handle("createTables")}
            >
              Create tables
            </Button>
            <Button
              variant="success"
              className="ms-2"
              onClick={() => handle("syncKHSDD")}
            >
              Sync KHSDD
            </Button>
          </Col>
        </Row>
      )}
    </Container>
  );
};

export async function getStaticProps() {
  return {
    props: { storeName: process.env.GEOSERVER_STORE },
  };
}

export default Layers;
