import { NextPage } from "next";
import { useRouter } from "next/router";
import { useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { apiClient } from "../lib/http_client";

const Login: NextPage = () => {
    const [key, setKey] = useState('');
    const [error, setError] = useState<string>();
    const router = useRouter();

    const submit = async () => {
        try {
            await apiClient.post('/api/user', { user: key });
            router.push('/');
        } catch (e) {
            setError('Wrong key');
        }
    }

    return (
        <Container>
            <Row className="mt-5">
                <Col md={{ span: 4, offset: 4 }} >
                    <div>
                        <Form.Group className="mb-3" controlId="formKey">
                            <Form.Label>Key</Form.Label>
                            <Form.Control value={key} onChange={(e) => setKey(e.target.value)} type="password" placeholder="Key" />
                            {error && <Form.Text className="text-danger">{error}</Form.Text>}
                        </Form.Group>
                        <Button variant="primary" onClick={submit}>
                            Submit
                        </Button>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}

export default Login;
