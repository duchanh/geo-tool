import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import dynamic from "next/dynamic";
import { WMSTileLayerProps } from "react-leaflet";
import { geoClient } from "../lib/http_client";

const Map = dynamic(() => import("../component/Map"), {
  ssr: false,
  suspense: false,
});

export default function Preview({
  wmsOptions,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  return <Map wmsOptions={wmsOptions} />;
}

export const getServerSideProps: GetServerSideProps<{
  wmsOptions?: WMSTileLayerProps;
}> = async (context) => {
  const { layer, type } = context.query;
  if (typeof layer === "string" && typeof type === "string") {
    const layerName = layer.includes(":") ? layer.split(":")[1] : layer;
    const wmsOptions = {
      url: process.env.GEOSERVER_URL + "/wms",
      layers: process.env.GEOSERVER_WORKSPACE + ":" + layerName,
      format: "image/png",
      transparent: true,
    } as WMSTileLayerProps;

    try {
      const resp = await geoClient.get(
        `/rest/workspaces/${process.env.GEOSERVER_WORKSPACE}/${type}s/${layerName}.json`
      );
      const bbox = resp.data[type].nativeBoundingBox;
      wmsOptions.bounds = [
        [bbox.miny, bbox.minx],
        [bbox.maxy, bbox.maxx],
      ];

      return {
        props: { wmsOptions },
      };
    } catch (e) {
      return { props: {} };
    }
  }
  return { props: {} };
};
