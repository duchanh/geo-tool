import {
  GetServerSideProps,
  InferGetServerSidePropsType,
  NextPage,
} from "next";
import ToolDbGis from "../../component/tooldbgis";
import { PATH_LOCATON } from "../../component/tooldbgis/const";
import { baseGetApi } from "../../component/tooldbgis/baseApi";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context;

  if (PATH_LOCATON[query.name as keyof typeof PATH_LOCATON]) {
    try {
      const res = await baseGetApi<any>({
        path: PATH_LOCATON[query.name as keyof typeof PATH_LOCATON].path,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
      });

      const data = res.results.map((item: any, index: number) => {
        return {
          ...item,
          number: index + 1,
        };
      });
      return {
        props: {
          results: data,
          totalResults: res.totalResults,
          name: query.name,
          mode: "list",
        },
      };
    } catch (err) {
      return {
        notFound: true,
      };
    }
  }
  return {
    notFound: true,
  };
};

const ToolDbGisPage = (pageProps: any) => {
  return <ToolDbGis {...pageProps} />;
};

export default ToolDbGisPage;
