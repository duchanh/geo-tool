import {
  GetServerSideProps,
  InferGetServerSidePropsType,
  NextPage,
} from "next";
import ToolDbGis from "../../../component/tooldbgis";
import { PATH_LOCATON } from "../../../component/tooldbgis/const";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context;

  if (PATH_LOCATON[query.name as keyof typeof PATH_LOCATON]) {
    return {
      props: {
        name: query.name,
        mode: "add",
        listField:
          PATH_LOCATON[query.name as keyof typeof PATH_LOCATON].listField,
      },
    };
  }
  return {
    notFound: true,
  };
};

const ToolDbGisPage = (pageProps: any) => {
  return <ToolDbGis {...pageProps} />;
};

export default ToolDbGisPage;
